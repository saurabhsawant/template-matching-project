#-------------------------------------------------
#
# Project created by QtCreator 2014-02-28T01:18:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ImageApp
TEMPLATE = app

INCLUDEPATH += C:\opencv\build\include

LIBS += C:\opencv\build\x86\vc10\lib\opencv_core248d.lib \
        C:\opencv\build\x86\vc10\lib\opencv_imgproc248d.lib \
        C:\opencv\build\x86\vc10\lib\opencv_highgui248d.lib \
        C:\opencv\build\x86\vc10\lib\opencv_ml248d.lib \
        C:\opencv\build\x86\vc10\lib\opencv_video248d.lib \
        C:\opencv\build\x86\vc10\lib\opencv_video248d.lib \
        C:\opencv\build\x86\vc10\lib\opencv_features2d248d.lib \
        C:\opencv\build\x86\vc10\lib\opencv_calib3d248d.lib \
        C:\opencv\build\x86\vc10\lib\opencv_objdetect248d.lib \
        C:\opencv\build\x86\vc10\lib\opencv_contrib248d.lib \
        C:\opencv\build\x86\vc10\lib\opencv_legacy248d.lib \
        C:\opencv\build\x86\vc10\lib\opencv_flann248d.lib

SOURCES += main.cpp\
        mainwindow.cpp \
    MyQGraphicScene.cpp

HEADERS  += mainwindow.h \
    MyQGraphicScene.h

FORMS    += mainwindow.ui
