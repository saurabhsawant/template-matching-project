#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "MyQGraphicScene.h"
#include <opencv/cv.h>

QT_BEGIN_NAMESPACE
class QLabel;
class QPoint;
class QGraphicsView;
class QGraphicsScene;
class QRubberBand;
class QGraphicsPixmapItem;
class QScrollArea;
class QGraphicsSceneMouseEvent;
QT_END_NAMESPACE


namespace Ui {
class app;
}

class app : public QMainWindow
{
    Q_OBJECT

public:
    explicit app(QWidget *parent = 0);
    ~app();

private slots:
    void on_image1Button_clicked();     //push button click events
    void on_image2Button_clicked();
    void on_matchButton_clicked();

private:
    Ui::app *ui;

    QGraphicsView* viewSourceImage;
    QGraphicsView* viewDestinationImage;

    MyQGraphicScene * sceneSourceImage;
    QGraphicsScene* sceneDestinationImage;

    cv::Mat matTemplate;
    cv::Mat matDestinationImage;
    cv::Mat matResult;
    QImage DestinationImage; // Destination image is added as object's member so that it can be used in matchButton slot
                             // Source image is NOT a member because it is not needed outside the image1Button slot where its created and added to QGraphicsScene
};

cv::Mat qimage2mat(const QImage& qimage);   //to convert QImage to openCV mat
#endif // MAINWINDOW_H
