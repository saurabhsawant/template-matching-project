#ifndef MyQGraphicScene_H
#define MyQGraphicScene_H

#include <QObject>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsTextItem>


QT_BEGIN_NAMESPACE
class QGraphicsSceneMouseEvent;
class QMenu;
class QPointF;
class QPoint;
class QGraphicsLineItem;
class QFont;
class QGraphicsTextItem;
class QRubberBand;
class QColor;
class QGraphicsPixmapItem;
class QGraphicsView;
class QGraphicsScene;
QT_END_NAMESPACE


//subclassing QGraphicsScene in order to crop image with mouse rubberbanding
class MyQGraphicScene : public QGraphicsScene
{
public:
    MyQGraphicScene();
    void getOwnCopy(QImage *ogImage);   //copies the image file loaded to scene as a private QImage member
    QImage getCroppedImage();           //returns the private QImage member
    bool cropped();                     //returns true/false depending on whether image was cropped or not
public slots:
    void mouseMoveEvent(QGraphicsSceneMouseEvent * event);      //reduntant. used during debuggings
    void mousePressEvent(QGraphicsSceneMouseEvent *event);      //gets co-ordinates of the click
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);    //gets co-ordinates of the release, draws rectangle over cropped area and crops QImage member as per the rectangle.

private:
    QPoint topLeft;                 //top left point of crop rectangle.
    QImage croppedImageFromOriginal;
    bool croppedFlag;
    //newView and newScene were used during developing and debugging! now redundant.
    //QGraphicsView* newView;
    //QGraphicsScene* newScene;
};

#endif // MyQGraphicScene_H
