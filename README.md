Template Matching using OpenCV

Development and Test Platform: Windows 64-bit Machine
IDE: QT Creator
Compiler: Microsoft Visual C++
OpenCV Version 2.4.8


How it works:
1. Executable opens a dialogue window with 3 buttons
   Source Image
   Target Image
   Match!

2. 'Source Image' and 'Target Image' buttons open a file dialogue to load the images.

3. Crop a section in opened 'Source Image' and press 'Match!' button to find the cropped section of 'source image' in the target image.

