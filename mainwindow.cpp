#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QPixmap>
#include <QLabel>
#include <QScrollArea>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QPoint>
#include <QRubberBand>
#include <QRect>
#include <QMouseEvent>

#include <opencv/cv.h>
//#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

app::app(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::app)
{
    ui->setupUi(this);
    sceneSourceImage = new MyQGraphicScene();
    sceneDestinationImage = new QGraphicsScene();
    viewSourceImage = new QGraphicsView(sceneSourceImage);
    viewDestinationImage = new QGraphicsView(sceneDestinationImage);
}

app::~app()
{
    delete ui;
}

void app::on_image1Button_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                    tr("Open File"), QDir::currentPath());
    if (!fileName.isEmpty()) {
        QImage image(fileName);
        if (image.isNull()) {
            QMessageBox::information(this, tr("Image Viewer"),
                                     tr("Cannot load %1.").arg(fileName));
            return;
        }

        QGraphicsPixmapItem * itemSourceImage = new QGraphicsPixmapItem(QPixmap::fromImage(image));

        sceneSourceImage->getOwnCopy(&image);
        sceneSourceImage->addItem(itemSourceImage);

        viewSourceImage->fitInView(sceneSourceImage->itemsBoundingRect() ,Qt::KeepAspectRatio);
        viewSourceImage->setDragMode(QGraphicsView::RubberBandDrag);
        viewSourceImage->setWindowTitle("Source Image");
        viewSourceImage->show();
        }
}

void app::on_image2Button_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                    tr("Open File"), QDir::currentPath());
    if (!fileName.isEmpty()) {
        QImage image(fileName);
        if (image.isNull()) {
            QMessageBox::information(this, tr("Image Viewer"),
                                     tr("Cannot load %1.").arg(fileName));
            return;
        }
        DestinationImage = image;
        QGraphicsPixmapItem* itemDestinationImage = new QGraphicsPixmapItem(QPixmap::fromImage(DestinationImage));
        sceneDestinationImage->addItem(itemDestinationImage);
        viewDestinationImage->fitInView(sceneDestinationImage->itemsBoundingRect() ,Qt::KeepAspectRatio);
        viewDestinationImage->setWindowTitle("Destination Image");
        viewDestinationImage->show();
        }
}

void app::on_matchButton_clicked()
{
        QImage croppedImage = sceneSourceImage->getCroppedImage();

        if (!sceneSourceImage->cropped()) {
            QMessageBox::information(this,tr("error"), tr("Location from source image not selected!"));
            return;
        }

        matTemplate = qimage2mat(croppedImage);
        matDestinationImage = qimage2mat(DestinationImage);

        // Create the result matrix
        int result_cols =  matDestinationImage.cols - matTemplate.cols + 1;
        int result_rows = matDestinationImage.rows - matTemplate.rows + 1;

        matResult.create( result_cols, result_rows, CV_32FC1 );

//        qDebug("Destination Image (w,h): %d %d, Cropped Image (w,h): %d %d Result Matrix (w,h): %d %d\n", \
//                matDestinationImage.cols, matDestinationImage.rows, \
//                matTemplate.cols, matTemplate.rows, \
//                matResult.cols, matResult.rows);

        matchTemplate( matDestinationImage, matTemplate, matResult, CV_TM_SQDIFF_NORMED );
//        matchTemplate( matDestinationImage, matTemplate, matResult, CV_TM_CCORR ); //CV_TM_CCOEFF or CV_TM_CCORR
        normalize( matResult, matResult, 0, 1, cv::NORM_MINMAX, -1, cv::Mat() );

        // Localizing the best match with minMaxLoc
        double minVal;
        double maxVal;
        cv::Point minLoc;
        cv::Point maxLoc;
        cv::Point matchLoc;

        minMaxLoc( matResult, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat() );

        matchLoc = minLoc; // for CV_TM_SQDIFF_NORMED
//        matchLoc = maxLoc;  // for CV_TM_CCOEFF or CV_TM_CCORR

//        cv::namedWindow( "result_window", CV_WINDOW_AUTOSIZE );
//        cv::imshow( "result_window", matResult );

        QGraphicsRectItem * rect = new QGraphicsRectItem(matchLoc.x, matchLoc.y, matTemplate.cols, matTemplate.rows);
        rect->setBrush(QBrush(Qt::black));
        rect->setOpacity(0.7);
        sceneDestinationImage->addItem(rect);
}

cv::Mat qimage2mat(const QImage& qimage) {
    cv::Mat mat = cv::Mat(qimage.height(), qimage.width(), CV_8UC4, (uchar*)qimage.bits(), qimage.bytesPerLine());
    cv::Mat mat2 = cv::Mat(mat.rows, mat.cols, CV_8UC3 );
    int from_to[] = { 0,0,  1,1,  2,2 };
    cv::mixChannels( &mat, 1, &mat2, 1, from_to, 3 );
    return mat2;
}
