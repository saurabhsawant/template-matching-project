#include <QtGui>
#include "MyQGraphicScene.h"
#include <QGraphicsSceneMouseEvent>
#include <QRubberBand>
#include <QGraphicsView>
#include <QPoint>
#include <QPointF>
#include <QRect>
#include <QRectF>

MyQGraphicScene::MyQGraphicScene()
{
    this->croppedFlag = false;
}

void MyQGraphicScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    qDebug("Mouse Detected!!\n");
    QPointF tempPointF = event->scenePos();
    topLeft.setX(tempPointF.x());
    topLeft.setY(tempPointF.y());

    qDebug("x-cordinate = %d and y-cordinate = %d of mouse press\n", topLeft.x(),topLeft.y());
}

void MyQGraphicScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    qDebug("Mouse Moving!!\n");
//    QPointF tempPointF = event->scenePos();

//    QPoint mouseMovePoint;
//    mouseMovePoint.setX(tempPointF.x());
//    mouseMovePoint.setY(tempPointF.y());
}

void MyQGraphicScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    qDebug("Mouse Released!!\n");

    QPointF tempPointF = event->scenePos();
    QPoint bottomRight;
    bottomRight.setX(tempPointF.x());
    bottomRight.setY(tempPointF.y());
    qDebug("x-cordinate = %d and y-cordinate = %d of mouse release\n", bottomRight.x(),bottomRight.y());

    //create rectangle of crop area
    QRect myRect(topLeft, bottomRight);

    QGraphicsRectItem * rect = new QGraphicsRectItem(topLeft.x(), topLeft.y(), myRect.width(), myRect.height());
    rect->setBrush(QBrush(Qt::white));
    rect->setOpacity(0.7);
    this->addItem(rect);                                                //add a rectangle to show cropped area
    croppedImageFromOriginal = croppedImageFromOriginal.copy(myRect);   //crop the image
    this->croppedFlag = true;                                           //set the flag that image is cropped
}

void MyQGraphicScene::getOwnCopy(QImage *ogImage)
{
    croppedImageFromOriginal = ogImage->copy();
}

QImage MyQGraphicScene::getCroppedImage()
{
    return croppedImageFromOriginal;
}

bool MyQGraphicScene::cropped()
{
    if(croppedFlag == true)
    {
        return true;
    }
    else
        return false;
}
